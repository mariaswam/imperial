<?php

App::uses('AppModel', 'Model');

/**
 * Register Model
 *
 * @property Place $Place
 */
class Competitor extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'nombre' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Este Campo es requerido',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'email' => array(
            'email' => array(
                'rule' => array('email'),
                'message' => 'Correo electrónico invalido',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Este Campo es requerido',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'telefono' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Este Campo es requerido',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'numeric' => array(
                'rule' => 'numeric',
                'message' => 'Por favor solo utilice números'
            )
        ),
        'cedula' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Este Campo es requerido',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

}
