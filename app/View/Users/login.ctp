<div class="col-md-12" style="color: #000">
    <div class="container">
        <div class="login-form white-box">
            <?php echo $this->Session->flash('auth'); ?>
            <?php echo $this->Form->create('User', array("class" => "form-horizontal")); ?>
            <div class="form-group">
                <?php echo $this->Form->input('username', array("div" => false, 'label' => 'Usuario', "class" => "form-control")); ?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->input('password', array("div" => false, 'label' => 'Password', "class" => "form-control")); ?>
            </div>
            <div class="row">
                <div class=col-md-6>        
                    <?php echo $this->Form->submit('Ingresar'); ?>
                </div>
            </div>        
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>
<div class="clearfix"></div>