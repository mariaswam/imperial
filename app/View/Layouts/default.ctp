<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo "Llegó la NUEVA Imperial"; ?>:
            <?php echo $this->fetch('title'); ?>
        </title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="https://imperial.cr/img/favicon/favicon-16x16.png">
        <?php
        echo $this->Html->css('bootstrap.min');
        echo $this->Html->css('main');

        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        ?>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

        <script src="https://s0.2mdn.net/ads/studio/cached_libs/tweenmax_2.1.2_min.js"></script>

        <!-- Facebook Pixel Code -->
        <script>
          !function(f,b,e,v,n,t,s)
          {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
          n.callMethod.apply(n,arguments):n.queue.push(arguments)};
          if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
          n.queue=[];t=b.createElement(e);t.async=!0;
          t.src=v;s=b.getElementsByTagName(e)[0];
          s.parentNode.insertBefore(t,s)}(window,document,'script',
          'https://connect.facebook.net/en_US/fbevents.js');
           fbq('init', '1930344080409693');
          fbq('track', 'PageView');
        </script>
        <noscript>
           <img height="1" width="1"
          src="https://www.facebook.com/tr?id=1930344080409693&ev=PageView
          &noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-136818121-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-136818121-1');
        </script>

    </head>

    <?php if ( $isVersionThree ): ?>

      <body class="<?php echo $activePage ?> beer-launch">
    <?php else: ?>
      <body class="<?php echo $activePage ?>">
    <?php endif; ?>

        <div id="container">
          <?php if ($activePage != 'ageGate'): ?>
            <div id="header">
                <div class="container">
                        <div class="row">
                                <div class="main-logo col-xs-6">
                                    <a href="<?php echo Router::url("/"); ?>">
                                      <?php if ( $isVersionThree ): ?>

                                        <?php echo $this->Html->image("lanzamiento/logo.png", array("class" => "img-responsive")); ?>
                                      <?php else: ?>
                                        <?php echo $this->Html->image("imperial-logo.png", array("class" => "img-responsive")); ?>
                                      <?php endif; ?>
                                    </a>
                                </div>
                        </div>
                </div>
            </div>
          <?php endif; ?>
            <div id="content">
                <?php echo $this->fetch('content'); ?>
            </div>
            <?php if ($activePage != 'ageGate'): ?>
              <?php echo $this->element("footer"); ?>
            <?php endif; ?>
            <div class="policy text-center black-color">Todos los Derechos Reservados 2019<br>CRPBA-2019-238</div>
        </div>

        <?php
        echo $this->Html->script('bootstrap.min');
        echo $this->Html->script('scripts');
//        echo $this->element('sql_dump');
        ?>



    </body>
</html>
