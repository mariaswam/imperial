<div class="registration-form success">
  <div class="form-heading">
      <?php echo $this->Html->image("imperial-logo.png", array("class" => "logo-section col-xs-3")); ?>
      <div class="col-xs-9 heading-text">
        <?php if ($from === "preorder"): ?>
          <span class="yellow-color">PRONTO TE LLEGARÁ UN CORREO</span>
          <span class="white-color"><b>CON LA CONFIRMACIÓN DE TU ORDEN</b></span>
        <?php endif; ?>
        <?php if($from === "participant"): ?>
          <span class="yellow-color">GRACIAS POR PARTICIPAR.</span>
          <span class="white-color"><b>PRONTO TENDREMOS A LOS GANADORES</b></span>
        <?php else: ?>
          <span class="yellow-color">GRACIAS POR REGISTRARTE.</span>
          <span class="white-color"><b>YA SOS PARTE DE LA COMUNIDAD IMPERIAL</b></span>
        <?php endif; ?>
      </div>
  </div>

  <div class="col-xs-12 form-body success" id="success">
      <div class="clear-fix"></div>
      <?php if ($from === "preorder"): ?>
        <span class="preorder">TU PRE-ORDEN</span>
        <span class="ha-sido">HA SIDO CREADA</span>
        <div class="yellow-color successful">
            Correctamente
        </div>
      <?php endif; ?>
      <?php if($from === "participant"): ?>
        <span class="preorder">YA ESTÁS</span>
        <div class="yellow-color successful">
            PARTICIPANDO
        </div>
      <?php else: ?>
        <span class="preorder">YA ESTÁS</span>
        <div class="yellow-color successful">
            REGISTRADO
        </div>
      <?php endif; ?>
  </div>
</div>

<script>
    (function () {


          $('.black-box').addClass('show-form');

    })();
</script>
