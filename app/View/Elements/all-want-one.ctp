<div class="special-row">
    <div class="col-xs-4 col-sm-3 col-sm-offset-1 col-md-4 beer-bottle-wrap">


        <?php if ( $isVersionThree ): ?>

          <?php echo $this->Html->image("lanzamiento/imperial-ultra.png", array("class" => "img-responsive ")); ?>
        <?php else: ?>
          <?php echo $this->Html->image("haft-beer.png", array("class" => "img-responsive haft-beer visible-md visible-lg")); ?>
          <?php echo $this->Html->image("half-beer-mobile.png", array("class" => "img-responsive haft-beer hidden-md hidden-lg")); ?>
        <?php endif; ?>

    </div>

      <?php if ( $isVersionThree ): ?>
        <div class="col-xs-8 col-sm-7 col-md-6  copy-wrap">

        <h2 class="title">¡BUSCALA YA<br><span>EN BARES,</span> SUPERMERCADOS Y LICORERAS DE TODO EL PAÍS!</h2>
        <p>Vení por tu Imperial Ultra en botella 350ml y lata 350ml.</p>

        <a href="https://lanueva.imperial.cr/files/CP_Imperial_Ultra.pdf" class="btn">Descargá el comunicado de prensa</a>

      <?php else: ?>
        <div class="col-xs-8 col-xs-offset-4 col-sm-7 col-sm-offset-4 col-md-6 col-md-offset-0 copy-wrap">
        <h2 class="yellow-color section-title">¡TODOS <br/>
            QUEREMOS<br/>
            UNA!</h2>
      <?php endif; ?>








    </div>

</div>
