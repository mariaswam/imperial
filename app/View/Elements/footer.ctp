<div id="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-2 footer-logo">

                <?php if ( $isVersionThree ): ?>

                  <?php echo $this->Html->image("lanzamiento/logo.png", array("class" => "img-responsive ")); ?>
                <?php else: ?>
                  <?php echo $this->Html->image("imperial-logo.png", array("class" => "img-responsive ")); ?>
                <?php endif; ?>
            </div>
            <div class="col-xs-12 col-md-3 footer-centered">
                <?php echo $this->Html->link('TÉRMINOS Y CONDICIONES', "/terminosycondiciones", array('class' => ($activePage == "rules" ? 'active' : ""))); ?>
            </div>
            <div class="col-xs-12 col-md-2 footer-centered">
              <?php if ($isVersionOne): ?>
                <?php echo $this->Html->link('REGLAMENTO', 'https://lanueva.imperial.cr/files/reglamento_preorden.pdf', array('class' => ($activePage == "rules" ? 'active' : ""), 'target' => '_blank')); ?>
              <?php else: ?>
                <?php echo $this->Html->link('REGLAMENTO', 'https://lanueva.imperial.cr/files/reglamento_rifa.pdf', array('class' => ($activePage == "rules" ? 'active' : ""), 'target' => '_blank')); ?>
              <?php endif; ?>
            </div>

            <div class="col-xs-12 col-md-2 footer-centered">
                <?php echo $this->Html->link('FORMULARIO', "/lanueva/#form", array('class' => '')); ?>
            </div>
            <div class="col-xs-12 col-md-3 footer-social">
                <div class="row">
                    <div class="col-md-4 social-label">SIGUENOS:</div>
                    <div class="col-md-8 social-links">
                        <a href="https://www.facebook.com/CervezaImperial/" target="_blank">
                            <?php echo $this->Html->image("footer-facebook.png", array("class" => "img-responsive")); ?>
                        </a>
                        <a href="https://www.instagram.com/cervezaimperialcr/" target="_blank">
                            <?php echo $this->Html->image("footer-instagram.png", array("class" => "img-responsive")); ?>
                        </a>
                        <a href="https://twitter.com/elguiaimperial?lang=es" target="_blank">
                            <?php echo $this->Html->image("footer-twitter.png", array("class" => "img-responsive")); ?>
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
