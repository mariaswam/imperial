<div class="form-heading">
    <?php echo $this->Html->image("imperial-logo.png", array("class" => "logo-section col-xs-3")); ?>
    <div class="col-xs-9 heading-text">

        <?php if ($isVersionOne): ?>
          <span class="yellow-color">LLENÁ EL FORMULARIO PARA</span>
          <span class="white-color"><b>PARA PRE-ORDENAR TU SIX</b></span>
        <?php elseif ($isVersionThree): ?>
          <span class="grey-color">
            ¿Querés ser uno de los primeros en enterarte de fiestas, conciertos o promos de Imperial?
          </span>
          <span class="white-color">
            <b>DEJANOS TUS DATOS</b>
          </span>
        <?php else: ?>
          <span class="yellow-color">LLENÁ EL FORMULARIO PARA</span>
          <span class="white-color"><b>PARA PARTICIPAR POR TU SIX</b></span>
        <?php endif; ?>
    </div>
</div>

<div class="col-xs-12 form-body">
    <?php echo $this->Form->create('Register', array('url' => array('controller' => 'Registers', 'action' => 'add')), array("class" => "form-horizontal")); ?>
    <div class="form-group">
        <?php echo $this->Form->input('nombre', array("div" => false, "label" => array("text" => "NOMBRE COMPLETO:*", "class" => "col-lg-12 control-label"), "class" => "form-control")); ?>
    </div>
    <div class="form-group">
        <?php echo $this->Form->input('cedula', array("div" => false, "label" => array("text" => "CÉDULA:*", "class" => "col-lg-12 control-label "), "class" => "form-control", "maxlength" => "12", "minlength" => "9")); ?>
    </div>
    <div class="form-group">
        <?php echo $this->Form->input('telefono', array("div" => false, "type" => "tel", "label" => array("text" => "TELÉFONO*", "class" => "col-lg-12 control-label "), "class" => "form-control phone", "maxlength" => "8")); ?>
    </div>
    <div class="form-group">
        <?php echo $this->Form->input('email', array("div" => false, "label" => array("text" => "CORREO*", "class" => "col-lg-12 control-label"), "class" => "form-control")); ?>
    </div>
    <?php if ($isVersionOne): ?>
    <div class="form-group ">
        <?php echo $this->Form->input('pick_up', array("options" => array("Tienda de la birra Heredia" => "TIENDA DE LA BIRRA HEREDIA", "Tienda de la birra SJO" => "TIENDA DE LA BIRRA SAN JOSÉ", "Centro Distribucion Nicoya" => "CENTRO DE DISTRIBUCIÓN NICOYA", "Centro Distribucion Liberia" => "CENTRO DE DISTRIBUCIÓN LIBERIA", "Centro Distribucion San Carlos" => "CENTRO DE DISTRIBUCIÓN SAN CARLOS", "Centro Distribucion Puntarenas" => "CENTRO DE DISTRIBUCIÓN PUNTARENAS", "Centro Distribucion Guapiles" => "CENTRO DE DISTRIBUCIÓN GUAPILES", "Centro Distribucion Limon" => "CENTRO DE DISTRIBUCIÓN LIMÓN", "Centro Distribucion Ciudad Neily" => "CENTRO DE DISTRIBUCIÓN CIUDAD NEILY", "Centro Distribucion San Isidro" => "CENTRO DE DISTRIBUCIÓN SAN ISIDRO"), "div" => false, "label" => array("text" => "¿DONDE QUIERO RETIRAR MI SIX?", "class" => "col-lg-12 control-label"), "class" => "form-control")); ?>
    </div>
    <?php endif; ?>
    <div class="form-group ">
        <div class="submit-btn-wrapper text-center">
            <div class="btn btn-black btn-rounded btn-submit main-submit" ><?php echo $isVersionOne ? "ORDENAR AHORA" : $isVersionThree ? "ENVIAR AHORA" : "PARTICIPAR AHORA"  ?></div>
            <p class="grey text-center uppercase">* Todos los campos son requeridos</p>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>

<script>
    (function () {
        function onlyAllowNumbers(e) {
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                    (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        }

        $('body').on('click', '.btn-go-to-form',function(e){
          e.preventDefault();

          $('.black-box').addClass('show-form');
          $('html, body').animate({
               scrollTop: $('.registration-form').offset().top - 70
           }, 800, function() {
               // if you need a callback function
           });

          var _tl = new TimelineMax();

          _tl
          .to('.promo', .2, { opacity: 0, ease: Sine.easeout })
          .to('.registration-form', 0, { opacity: 1, ease: Sine.easeIn })



        });

        $(".main-submit").click(function () {
          if ($('#RegisterNombre').val() == '') {
            alert('Debe ingresar un nombre válido');
            return;
          }
          if ($('#RegisterTelefono').val() == '' || $('#RegisterTelefono').val().length < 8) {
            alert('Debe ingresar un teléfono válido');
            return;
          }
          if (!validEmail($('#RegisterEmail').val())) {
            alert('Debe ingresar un correo electrónico válido');
            return;
          }
          if($('#RegisterCedula').val().length >= 9){
            $("#RegisterAddForm").submit();
          }
          else{
            alert('Ingrese una cédula válida (9 dígitos cédulas nacionales - 12 cédulas de residencia)');
          }
        });

        $("#RegisterNumIdentificacion1,#RegisterNumIdentificacion2,#RegisterNumIdentificacion3,#RegisterTelefono,#RegisterCedula").keydown(function (e) {
            onlyAllowNumbers(e);
        });
        $("#RegisterNumIdentificacion1,#RegisterNumIdentificacion2,#RegisterNumIdentificacion3,#RegisterTelefono").focusin(function (e) {
            $(this).val("");
        });

        $("#RegisterTelefono").keyup(function (e) {
            if ($(this).val().length >= 8) {
                $("#RegisterTipoIdentificacion").focus();
            }
        });

        $("#RegisterNumIdentificacion1").keyup(function (e) {
            if ($(this).val().length >= 1) {
                $("#RegisterNumIdentificacion2").focus();
            }
        });
        $("#RegisterNumIdentificacion2").keyup(function (e) {
            if ($(this).val().length >= 4) {
                $("#RegisterNumIdentificacion3").focus();
            }
        });
        $("#RegisterNumIdentificacion3").keyup(function (e) {
            if ($(this).val().length >= 4) {
                $("#RegisterNumFactura").focus();
            }
        });
    })();

    function validEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

      return re.test(String(email).toLowerCase());
    }
</script>
