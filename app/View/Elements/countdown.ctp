<?php
$showForm = isset($showForm) ? $showForm : false;
$success = isset($success) ? $success : false;
$error = isset($error) ? $error : false;
$message = isset($message) ? $message : false;
$from = isset($from) ? $from : false;
?>
<?php if ($isVersionThree): ?>

<?php else: ?>
<div class="yellow-box hidden-md hidden-lg want-mine">
  <?php if ($isVersionOne): ?>
    <span class="black-color i-want"><b>QUIERO LA MÍA</b></span>
    <?php else: ?>
      <span class="black-color i-want"><b>PARTICIPAR POR LA MÍA</b></span>
  <?php endif; ?>
  <?php if ($from || $message):?>
    <?php echo $this->Html->link('Click aquí', '/lanueva', array('class' => "btn btn-black btn-rounded btn-go-to-form")); ?>
  <?php else:?>
    <?php echo $this->Html->link('Click aquí', '', array('class' => "btn btn-black btn-rounded btn-go-to-form")); ?>
  <?php endif; ?>

  <?php if ($isVersionOne): ?>
  <div class="hero-ramainig">

      <div class="col-xs-12">
          <div class="remaining-beers">
              <div class="beer-remainig-level">
                  <?php echo $this->Html->image("beer-level.png", array("class" => "beer-level img-responsive")); ?>

                  <div class="beer-remainig-level-black" style="height: <?php echo (1000 - $currentPreorderAmount) / 10 ?>%"></div>
              </div>
          </div>
          <span class="remaining-beers-labels">
              <span >SIX PACK</span><br/>
              <span>RESTANTES</span><br/>
              <span class="remaining-amount"><b><?php echo 1000 - $currentPreorderAmount ?></b></span>
          </span>
      </div>

  </div>
  <?php endif; ?>
</div>
<?php endif; ?>
<div class="row">
  <div class="countdown-wrapper">


    <div class="col-md-5 black-box-wrapper">
        <div class="black-box ">
            <?php
            if ($success) {
                echo $this->element("success", array("from" => $from));
            } else if ($error){
              echo $this->element("repeat-error", array("message" => $message));
            } else {
                ?>
                <div class="registration-form">
                  <?php echo $this->element("registrationForm"); ?>
                </div>

                <?php if ($isVersionThree): ?>


                <?php else: ?>

                  <div class="promo">
                    <?php echo $this->Html->image("imperial-logo.png", array("class" => "logo-section")); ?>
                    <div class="clear-fix"></div>

                    <div class="text-wrapper">



                      <?php if ($isVersionOne): ?>
                        <span class="preorder">PRE-ORDENÁ</span>

                        <span class="one-of">UNO DE LOS</span>
                        <span class="thousand-six yellow-color">
                            1000 SIX
                        </span>
                        <div class="of-new yellow-color">
                            DE LA NUEVA
                        </div>

                      <?php else: ?>
                        <span class="one-of small">LOS 1000 SIX</span>
                        <span class="preorder white small">SE FUERON <strong>VOLANDO</strong></span>
                        <span class="preorder small">PERO TENÉS EL CHANCE DE</span>
                        <span class="preorder small">PARTICIPAR POR UNO DE LOS</span>
                        <span class="thousand-six yellow-color">
                            300 SIX
                        </span>
                        <div class="of-new yellow-color">
                            DE LA NUEVA
                        </div>
                      <?php endif; ?>
                      <?php echo $this->Html->image("imperial-label.png", array("class" => "img-responsive imperial-label")); ?>


                    </div>
                  </div>
                <?php endif; ?>

            <?php } ?>
        </div>
    </div>

      <?php if ($isVersionThree): ?>
        <div class="col-md-6 left-flex-container video-wrapper">
          <?php echo $this->Html->image("lanzamiento/banner.jpg", array("class" => "img-responsive")); ?>
      <?php else: ?>
        <div class="col-md-6 left-flex-container counter-wrapper">
          <div class="yellow-box visible-md visible-lg">

            <?php if ($isVersionOne): ?>
              <span class="black-color i-want"><b>QUIERO LA MÍA</b></span>
              <?php else: ?>
                <span class="black-color i-want"><b>PARTICIPAR POR LA MÍA</b></span>
            <?php endif; ?>
            <?php if ($from || $message):?>
              <?php echo $this->Html->link('Click aquí', '/lanueva', array('class' => "btn btn-black btn-rounded btn-go-to-form")); ?>
            <?php else:?>
              <?php echo $this->Html->link('Click aquí', '', array('class' => "btn btn-black btn-rounded btn-go-to-form")); ?>
            <?php endif; ?>

            <?php if ($isVersionOne): ?>
            <div class="hero-ramainig">

                <div class="col-xs-10 col-xs-offset-1 col-sm-12">
                    <div class="remaining-beers">
                        <div class="beer-remainig-level">
                            <?php echo $this->Html->image("beer-level.png", array("class" => "beer-level img-responsive")); ?>

                            <div class="beer-remainig-level-black" style="height: <?php echo (1000 - $currentPreorderAmount) / 10 ?>%"></div>
                        </div>
                    </div>
                    <span class="remaining-beers-labels">
                        <span >SIX PACK</span><br/>
                        <span>RESTANTES</span><br/>
                        <span class="remaining-amount"><b><?php echo 1000 - $currentPreorderAmount ?></b></span>
                    </span>
                </div>

            </div>
            <?php endif; ?>
          </div>
          <div class="white-box">
              <div>
                  <strong class="black-color remaining-time">FALTA PARA CONOCERLA</strong>
              </div>
              <div style="" class="black-color remaining-time-clock">
                      <span class="numbers-wrap">
                        <strong class="days countdown-number"></strong>
                        <span class="remaining-time-clock-labels">DIAS</span>
                      </span>

                      <span class="numbers-wrap">
                        <strong class="hours countdown-number"></strong>
                        <span class="remaining-time-clock-labels">HORAS</span>
                      </span>

                      <span class="numbers-wrap">
                        <strong class="minutes  countdown-number"></strong>
                        <span class="remaining-time-clock-labels">MINUTOS</span>
                      </span>

                      <span class="numbers-wrap">
                        <strong class="seconds countdown-number"></strong>
                        <span class="remaining-time-clock-labels">SEGUNDOS</span>
                      </span>

              </div>

          </div>

      <?php endif; ?>
    </div>
  </div>
</div>


<script>
    $(function () {
        let countDownDate = new Date("Apr 1, 2019 12:00:00").getTime();
        var x = setInterval(function () {

            // Get todays date and time
            var now = new Date().getTime();
            // Find the distance between now and the count down date
            var distance = countDownDate - now;
            // Time calculations for days, hours, minutes and seconds
            function checkDays() {
              var amount = Math.floor(distance / (1000 * 60 * 60 * 24))

              if ( amount < 10 ) {
                amount = '0' + amount;
              }

              return amount;
            }

            var days = checkDays() + ':';
            var hours = ("0" + Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))).slice(-2) + ':';
            var minutes = ("0" + Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))).slice(-2) + ':';
            var seconds = ("0" + Math.floor((distance % (1000 * 60)) / (1000))).slice(-2);

            $(".remaining-time-clock .days").text(days);
            $(".remaining-time-clock .hours").text(hours);
            $(".remaining-time-clock .minutes").text(minutes);
            $(".remaining-time-clock .seconds").text(seconds);
            // If the count down is over, write some text
            if (distance < 0) {
                clearInterval(x);
            }
        }, 1000);
    });
</script>
