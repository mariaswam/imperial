<?php
  $message = isset($message) ? $message : false;
?>
<?php echo $this->Html->image("imperial-logo.png", array("class" => "logo-section col-xs-4 col-xs-offset-4")); ?>
<div class="col-xs-12 form-body success">
    <div class="clear-fix"></div>
    <span class="preorder">¡LO SENTIMOS!</span> <br/>
    <span class="ha-sido"><?= $message ?></span>
</div>
