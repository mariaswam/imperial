<div class="hero-wrapper" >
    <div class="hero">
        <div class="beer-hero">

          <?php if ($isVersionThree): ?>
            <?php echo $this->Html->image("lanzamiento/main-banner.jpg", array("class" => "img-responsive visible-md visible-lg")); ?>
            <?php echo $this->Html->image("lanzamiento/main-banner-mobile.jpg", array("class" => "img-responsive hidden-md hidden-lg")); ?>
          <?php else: ?>
            <?php echo $this->Html->image("beer.png", array("class" => "beer img-responsive")); ?>
            <?php echo $this->Html->image("beer-water.png", array("class" => "beer-water ")); ?>
            <?php echo $this->Html->image("beer-lines.png", array("class" => "beer-lines ")); ?>
            <?php echo $this->Html->image("llego-la-nueva.png", array("class" => "imperial img-responsive")); ?>
            <?php if ($isVersionOne): ?>
              <a class="hero-btn"> <?php echo $this->Html->image("hero-button.png", array("class" => "")); ?></a>
            <?php endif; ?>
          <?php endif; ?>
        </div>

    </div>

</div>



<script>
    $(function () {
      $('body').on('click', '.hero-btn',function(e){
        e.preventDefault();
        $('html, body').animate({
             scrollTop: $('#form').offset().top - 20
         }, 800, function() {
             // if you need a callback function
         });
      });
    });
</script>
