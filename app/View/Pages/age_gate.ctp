<div class="container">
  <div class="row row-content">
    <div class="col-md-4 col-xs-12">
      <?php echo $this->Html->image("imperial-logo-black.png", array("class" => "img-responsive")); ?>
    </div>
    <div class="col-md-8 col-xs-12">
      <h2 class="yellow-color">HOLA,</h2>
      <h3>ANTES DE SEGUIR ADELANTE NECESITAMOS <br> CONFIRMAR QUE SOS <strong>MAYOR DE EDAD.</strong></h3>
      <div class="row">
        <div class="col-md-4">
          <select class="form-control day-select" name="day" id="daySelect">
            <option value="0">DÍA</option>
            <?php for ($i=1; $i <= 31; $i++) { ?>
              <option value="<?= $i ?>"><?= $i ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="col-md-4">
          <select class="form-control day-select" name="month" id="monthSelect">
            <option value="0">MES</option>
            <option value="01">ENERO</option>
            <option value="02">FEBRERO</option>
            <option value="03">MARZO</option>
            <option value="04">ABRIL</option>
            <option value="05">MAYO</option>
            <option value="06">JUNIO</option>
            <option value="07">JULIO</option>
            <option value="08">AGOSTO</option>
            <option value="09">SEPTIEMBRE</option>
            <option value="10">OCTUBRE</option>
            <option value="11">NOVIEMBRE</option>
            <option value="12">DICIEMBRE</option>
          </select>
        </div>
        <div class="col-md-4">
          <select class="form-control day-select" name="year" id="yearSelect">
            <option value="0">AÑO</option>
            <?php for ($i=0; $i < 90; $i++) { ?>
              <option value="<?= 2019-$i ?>"><?= 2019-$i ?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      <button type="button" class="btn btn-age" id="submitAge" name="button">ENTRAR</button>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function () {
    $('#submitAge').click(function() {
      if ($('#daySelect').val() == 0 || $('#monthSelect').val() == 0 || $('#yearSelect').val() == 0) {
        alert('Debe ingresar una fecha válida.');
      }
      else{
        var today = new Date();
        var todayYear = today.getUTCFullYear();
        var todayMonth = today.getUTCMonth() + 1;
        var todayDay = today.getUTCDate();

        var age = todayYear - $('#yearSelect').val();
        var monthDifference = todayMonth - $('#monthSelect').val();
        if (monthDifference < 0 || (monthDifference === 0 && todayDay < $('#daySelect').val())) {
           age--;
        }

        if (age > 18) {
          location.href = "/imperial/lanueva";
        }
        else{
          alert('Debe ser mayor de edad para ingresar al sitio.');
        }
      }
    });
  });
</script>
