<?php ?>
<div class="home">
  <div class=" grey-section ">
      <div class="container">


        <?php
        $showForm = isset($showForm) ? $showForm : false;
        $success = isset($success) ? $success : false;
        ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="black-box rules">
                    <div class="form-heading">
                        <?php echo $this->Html->image("imperial-logo.png", array("class" => "col-xs-1")); ?>
                        <div class="col-xs-9 heading-text">
                            <span class="yellow-color"><b>TÉRMINOS Y CONDICIONES</b></span>
                        </div>
                    </div>
                    <div class="col-xs-12 rules-body">
                        <p>Al aceptar el presente formulario, acepto que Distribuidora La Florida S.A. con cédula de persona jurídica número 3-101-295868 (En adelante denominado “La Empresa”) podría recopilar y dar tratamiento a información de datos personales de acceso irrestricto y/o de acceso restringido, para lo cual, manifiesto tener pleno conocimiento que los datos brindados podrán ser incluidos en una base de datos cuyo titular es La Empresa y que los mismos podrán ser almacenados afuera de Costa Rica. El nombre de la base de datos en la cual se almacenará dicha información es La Nueva Imperial. Asimismo, los datos brindados podrían ser utilizados con fines de prospección comercial, enviarme información sobre futuras promociones de La Empresa y ofrecerme información sobre nuevos productos, publicidad así como promociones y actividades comerciales que realice La Empresa. La facilitación de estos datos es facultativa, sin embargo, en caso de no brindarlas, no podré participar en la presente promoción. Para ejercer los derechos de acceso, rectificación, supresión y revocación, así como cualquier otro derecho que tengo derecho, fui informado que puedo llamar al 800-FLORIDA.
                        </p>
                        <p>
                          Entiendo los alcances de la autorización aquí otorgada y sus implicaciones y doy mi CONSENTIMIENTO LIBRE EXPRESO E INFORMADO para que la Empresa use la información recopilada para lo descrito en el presente documento.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
