<?php ?>
<div class="home">
  <div class=" grey-section ">
      <div class="container">


        <?php
        $showForm = isset($showForm) ? $showForm : false;
        $success = isset($success) ? $success : false;
        ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="black-box rules">
                    <div class="form-heading">
                        <?php echo $this->Html->image("imperial-logo.png", array("class" => "col-xs-1")); ?>
                        <div class="col-xs-9 heading-text">
                            <span class="yellow-color"><b>REGLAMENTO</b></span>
                        </div>
                    </div>
                    <div class="col-xs-12 rules-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
