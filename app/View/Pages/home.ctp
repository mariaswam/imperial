<?php ?>
<div class="home">
    <div class="container">
        <?php echo $this->element("beer-hero"); ?>
    </div>
    <div class="grey-section" id="form">
      <div class="container">
        <?php echo $this->element("countdown"); ?>
      </div>

    </div>
    <div class="black-section ">
      <div class="container ">
        <?php echo $this->element("all-want-one"); ?>
      </div>

    </div>
</div>
