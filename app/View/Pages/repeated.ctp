<?php
$message = '';
  if ($_GET['repeated'] == "cedula") {
    $message = "La cédula ya se encuentra registrada.";
  }
  else if($_GET['repeated'] == "email"){
    $message = "El correo electrónico ya se encuentra registrado.";
  }
?>
<div class="home">
    <div class="container">
        <?php echo $this->element("beer-hero"); ?>
    </div>
    <div class="grey-section " id="form">
      <div class="container">
        <?php echo $this->element("countdown", array("error" => true, "message" => $message)); ?>
      </div>

    </div>
    <div class="black-section ">
      <div class="container">
        <?php echo $this->element("all-want-one"); ?>
      </div>

    </div>
</div>
