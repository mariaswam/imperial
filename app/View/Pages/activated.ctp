<?php
$from = isset($_GET['order']) ? $_GET['order'] : '';
?>
<div class="home">
    <div class="row">
        <?php echo $this->element("beer-hero"); ?>
    </div>
    <div class="grey-section " id="form">
      <div class="container">
        <?php echo $this->element("countdown", array("success" => true, "from" => $from)); ?>
      </div>

    </div>
    <div class="black-section ">
      <div class="container">
        <?php echo $this->element("all-want-one"); ?>
      </div>

    </div>
</div>
