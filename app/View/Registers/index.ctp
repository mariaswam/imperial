<div class="registers-list col-xs-12">
    <h2>Lista de registros <?php echo $this->Html->link('Salir', array("controller" => "users", "action" => "logout"), array("class" => "btn btn-danger pull-right")); ?>    </h2>
    <?php echo $this->Html->link('Descargar CSV', array("action" => "export"), array("target" => "_blank", "class" => "btn btn-primary btn-lg")); ?>  
    <table class="table" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th ><?php echo $this->Paginator->sort('id'); ?></th>
                <th class="col-xs-2"><?php echo $this->Paginator->sort('nombre'); ?></th>
                <th class="col-xs-2"><?php echo $this->Paginator->sort('email'); ?></th>
                <th class="col-xs-1"><?php echo $this->Paginator->sort('telefono'); ?></th>
                <th class="col-xs-2"><?php echo $this->Paginator->sort('tipo_identificacion', "Tipo de Identificacion"); ?></th>
                <th class="col-xs-1"><?php echo $this->Paginator->sort('num_identificacion', "Numero Identificacion"); ?></th>
                <th class="col-xs-2"><?php echo $this->Paginator->sort('place_id', "Musi donde compraste"); ?></th>
                <th ><?php echo $this->Paginator->sort('num_factura', "Numero de factura"); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($registers as $register): ?>
                <tr>
                    <td><?php echo h($register['Register']['id']); ?>&nbsp;</td>
                    <td><?php echo h($register['Register']['nombre']); ?>&nbsp;</td>
                    <td><?php echo h($register['Register']['email']); ?>&nbsp;</td>
                    <td><?php echo h($register['Register']['telefono']); ?>&nbsp;</td>
                    <td><?php echo h($register['Register']['tipo_identificacion']); ?>&nbsp;</td>
                    <td><?php echo h($register['Register']['num_identificacion']); ?>&nbsp;</td>
                    <td>
                        <?php echo h($register['Place']['nombre']); ?>
                    </td>
                    <td><?php echo h($register['Register']['num_factura']); ?>&nbsp;</td>
                    <td class="actions">
                        <?php echo $this->Form->postLink(__('Borrar'), array('action' => 'delete', $register['Register']['id']), array('confirm' => __('Esta reguro que desea eliminar este registro?'))); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <nav aria-label="paginacion">
        <ul class="pagination view-pagination">
            <li><?php echo $this->Paginator->first('Inicio'); ?></li>
            <li><?php echo $this->Paginator->prev('<', array(), null, array('class' => 'disabled')); ?></li>
            <?php
            echo $this->Paginator->numbers(array(
                'tag' => 'li',
                'separator' => false,
                'currentClass' => 'active',
                'currentTag' => 'a'
            ));
            ?>
            <li><?php echo $this->Paginator->next('>', array(), null, array('class' => 'disabled')); ?></li>
            <li><?php echo $this->Paginator->last('Último'); ?></li>
        </ul>
    </nav>
</div>
<div class="clearfix"></div>
