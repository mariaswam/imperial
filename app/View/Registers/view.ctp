<div class="registers view">
<h2><?php echo __('Register'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($register['Register']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nombre'); ?></dt>
		<dd>
			<?php echo h($register['Register']['nombre']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($register['Register']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Telefono'); ?></dt>
		<dd>
			<?php echo h($register['Register']['telefono']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tipo Identificacion'); ?></dt>
		<dd>
			<?php echo h($register['Register']['tipo_identificacion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Num Identificacion'); ?></dt>
		<dd>
			<?php echo h($register['Register']['num_identificacion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Place'); ?></dt>
		<dd>
			<?php echo $this->Html->link($register['Place']['id'], array('controller' => 'places', 'action' => 'view', $register['Place']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Num Factura'); ?></dt>
		<dd>
			<?php echo h($register['Register']['num_factura']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Register'), array('action' => 'edit', $register['Register']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Register'), array('action' => 'delete', $register['Register']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $register['Register']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Registers'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Register'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Places'), array('controller' => 'places', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Place'), array('controller' => 'places', 'action' => 'add')); ?> </li>
	</ul>
</div>
