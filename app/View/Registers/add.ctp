<?php ?>
<div class="home">
    <div class="row">
        <?php echo $this->element("beer-hero"); ?>
    </div>
    <div class="row grey-section ">
        <?php echo $this->element("countdown", array("showForm" => true)); ?>
    </div>
    <div class="row black-section ">
        <?php echo $this->element("all-want-one"); ?>
    </div>
</div>