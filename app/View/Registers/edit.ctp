<div class="registers form">
<?php echo $this->Form->create('Register'); ?>
	<fieldset>
		<legend><?php echo __('Edit Register'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('nombre');
		echo $this->Form->input('email');
		echo $this->Form->input('telefono');
		echo $this->Form->input('tipo_identificacion');
		echo $this->Form->input('num_identificacion');
		echo $this->Form->input('place_id');
		echo $this->Form->input('num_factura');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Register.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Register.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Registers'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Places'), array('controller' => 'places', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Place'), array('controller' => 'places', 'action' => 'add')); ?> </li>
	</ul>
</div>
