<div class="places view">
<h2><?php echo __('Place'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($place['Place']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nombre'); ?></dt>
		<dd>
			<?php echo h($place['Place']['nombre']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Place'), array('action' => 'edit', $place['Place']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Place'), array('action' => 'delete', $place['Place']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $place['Place']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Places'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Place'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Registers'), array('controller' => 'registers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Register'), array('controller' => 'registers', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Registers'); ?></h3>
	<?php if (!empty($place['Register'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Nombre'); ?></th>
		<th><?php echo __('Email'); ?></th>
		<th><?php echo __('Telefono'); ?></th>
		<th><?php echo __('Tipo Identificacion'); ?></th>
		<th><?php echo __('Num Identificacion'); ?></th>
		<th><?php echo __('Place Id'); ?></th>
		<th><?php echo __('Num Factura'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($place['Register'] as $register): ?>
		<tr>
			<td><?php echo $register['id']; ?></td>
			<td><?php echo $register['nombre']; ?></td>
			<td><?php echo $register['email']; ?></td>
			<td><?php echo $register['telefono']; ?></td>
			<td><?php echo $register['tipo_identificacion']; ?></td>
			<td><?php echo $register['num_identificacion']; ?></td>
			<td><?php echo $register['place_id']; ?></td>
			<td><?php echo $register['num_factura']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'registers', 'action' => 'view', $register['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'registers', 'action' => 'edit', $register['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'registers', 'action' => 'delete', $register['id']), array('confirm' => __('Are you sure you want to delete # %s?', $register['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Register'), array('controller' => 'registers', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
