<div class="places form">
<?php echo $this->Form->create('Place'); ?>
	<fieldset>
		<legend><?php echo __('Edit Place'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('nombre');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Place.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Place.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Places'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Registers'), array('controller' => 'registers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Register'), array('controller' => 'registers', 'action' => 'add')); ?> </li>
	</ul>
</div>
