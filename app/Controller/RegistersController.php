<?php

App::uses('AppController', 'Controller');
App::import('Model', 'Competitor');
App::import('Model', 'Target');

/**
 * Registers Controller
 *
 * @property Register $Register
 * @property PaginatorComponent $Paginator
 */
class RegistersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $name = 'Registers';
    public $helpers = array('Html', 'Form', 'Session', 'Paginator');
    public $components = array('Session', 'RequestHandler');
    public $paginate = array(
        'limit' => 30,
    );

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('add');
    }

    public function export() {
        $data = $this->Register->find("all", array("fields" => "Register.nombre,Register.email,Register.telefono,Register.tipo_identificacion,Register.num_identificacion,Register.num_factura,Place.nombre"));
        $_serialize = 'data';
        $_header = array('Nombre', 'email', 'telefono', 'tipo_identificacion', "num_identificacion", "num_factura", "Musi donde compraste");
        $_extract = array("Register.nombre", "Register.email", "Register.telefono", "Register.tipo_identificacion", "Register.num_identificacion", "Register.num_factura", "Place.nombre");

        $this->viewClass = 'CsvView.Csv';
        $this->set(compact('data', '_serialize', '_header', '_extract'));
    }

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->Register->recursive = 0;
        $this->set('registers', $this->paginate('Register'));
        $this->set("activePage", "index");
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Register->exists($id)) {
            throw new NotFoundException(__('Invalid register'));
        }
        $options = array('conditions' => array('Register.' . $this->Register->primaryKey => $id));
        $this->set('register', $this->Register->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        $errorOnIdentification = false;
        if ($this->request->is('post')) {
          //In case that six packs are sold out
          $this->Competitor = new Competitor();
          $this->Target = new Target();
          $lastDate = new DateTime("04/01/2019 12:00");

          if (new DateTime() > $lastDate) {
            $targetEmail = $this->Target->findByEmail($this->request->data['Register']['email']);
            $targetID = $this->Target->findByCedula($this->request->data['Register']['cedula']);

            if (!$targetID && !$targetEmail) {
              $this->Target->set([
                                  'nombre' => $this->request->data['Register']['nombre'],
                                  'email' => $this->request->data['Register']['email'],
                                  'telefono' => $this->request->data['Register']['telefono'],
                                  'cedula' => $this->request->data['Register']['cedula']
                                ]);

              if ($this->Target->save()) {
                $data = $this->request->data['Register'];
                $this->sendDataMailchimp($data, 'NL');
                return $this->redirect("/registroExitoso?order=target#success");
              }
            }
            else{
              return $targetID ? $this->redirect("/registroReplicado?repeated=cedula") : $this->redirect("/registroReplicado?repeated=email");
            }
          }
          else {
            //Check if identification number or email already exists
            $userID = $this->Register->findByCedula($this->request->data['Register']['cedula']);
            $userEmail = $this->Register->findByEmail($this->request->data['Register']['email']);

            if (!$userID && !$userEmail) {
              //Check if still are six packs
              $numberOfRegisters = $this->Register->find('count');
              if ($numberOfRegisters == NUMBER_OF_SIX_PACKS) {
                //Check for user
                $participantEmail = $this->Competitor->findByEmail($this->request->data['Register']['email']);
                $participantID = $this->Competitor->findByCedula($this->request->data['Register']['cedula']);

                if (!$participantID && !$participantEmail) {
                  //Save for participants another list in mailchimp
                  $this->Competitor->set(['nombre' => $this->request->data['Register']['nombre'],
                                          'email' => $this->request->data['Register']['email'],
                                          'telefono' => $this->request->data['Register']['telefono'],
                                          'cedula' => $this->request->data['Register']['cedula']]);

                  if ($this->Competitor->save($this->request->data)) {
                    $data = $this->request->data['Register'];
                    $this->sendDataMailchimp($data, 'PP');
                    return $this->redirect("/registroExitoso?order=participant#success");
                  }
                  else{
                    return $this->redirect("/");
                  }
                }
                else{
                  if ($participantID) {
                    return $this->redirect("/registroReplicado?repeated=cedula");
                  }
                  else{
                    return $this->redirect("/registroReplicado?repeated=email");
                  }
                }
              }
              else{
                //Save pre-order six pack
                $this->Register->create();
              if ($this->Register->save($this->request->data)) {
                $data = $this->request->data['Register'];
                  $this->sendDataMailchimp($data, 'PO');
                    return $this->redirect("/registroExitoso?order=preorder#success");
                }
              }
            }
            else {
              if ($userID) {
                return $this->redirect("/registroReplicado?repeated=cedula");
              }
              else{
                return $this->redirect("/registroReplicado?repeated=email");
              }
            }
          }
        }
        $this->set("activePage", "home");
    }

    private function getNumIdentification() {
        return $this->request->data["Register"]["num_identificacion1"] . $this->request->data["Register"]["num_identificacion2"]
                . $this->request->data["Register"]["num_identificacion3"];
    }

    private function validIdentification() {
        if (strlen($this->request->data["Register"]["num_identificacion1"]) == 0 ||
                strlen($this->request->data["Register"]["num_identificacion2"]) == 0 ||
                strlen($this->request->data["Register"]["num_identificacion3"]) == 0) {
            return false;
        }
        return true;
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Register->exists($id)) {
            throw new NotFoundException(__('Invalid register'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Register->save($this->request->data)) {
                $this->Flash->success(__('The register has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The register could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Register.' . $this->Register->primaryKey => $id));
            $this->request->data = $this->Register->find('first', $options);
        }
        $places = $this->Register->Place->find('list');
        $this->set(compact('places'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Register->id = $id;
        if (!$this->Register->exists()) {
            throw new NotFoundException(__('Invalid register'));
        }
        $this->request->allowMethod('post', 'delete');
        $this->Register->delete();
        return $this->redirect(array('action' => 'index'));
    }

    private function sendDataMailchimp($data, $type){
      $API_KEY = "56d43c21471634de32cbd6e57405f10a-us20";
      $list_id = $type == "PO" ? "c94a1b8476" : "37ebde1ed4";
      $list_id = $type == "NL" ? "d6c40d3a4a" : $list_id;

      if ($type == "PO") {
        $dataToSend = [
          "email_address" => $data['email'],
          "status" => "subscribed",
          "merge_fields" => [
            "FNAME" => $data['nombre'],
            "PHONE" => $data['telefono'],
            "IDNUMBER" => $data['cedula'],
            "PICKUP" => $data['pick_up']
          ]
        ];
      }
      else{
        $dataToSend = [
          "email_address" => $data['email'],
          "status" => "subscribed",
          "merge_fields" => [
            "FNAME" => $data['nombre'],
            "PHONE" => $data['telefono'],
            "IDNUMBER" => $data['cedula']
          ]
        ];
      }

      $ch = curl_init('https://us20.api.mailchimp.com/3.0/lists/'.$list_id.'/members/');
      curl_setopt_array($ch, array(
        CURLOPT_POST => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HTTPHEADER => array(
            'Authorization: Basic '.$API_KEY,
            'Content-Type: application/json'
        ),
        CURLOPT_POSTFIELDS => json_encode($dataToSend)
      ));

      $response = curl_exec($ch);
    }

}
